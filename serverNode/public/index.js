
function getGamesData() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let gameData = JSON.parse(xhttp.response)
            loadData(gameData)
        }
    };
    xhttp.open("GET", "http://96d9d3fb5e9e.ngrok.io/", true);
    xhttp.send();
}

function loadData(gamesData) {
    gamesData = eval(gamesData)
    console.log(typeof gamesData) 
    var content = '';
    gamesData.forEach(element => {
        var card = '';
        card += `<div class=\'card\'><img src=\'`;
        card += element.image;
        card += `\' alt=\'Game image\' style=\'width:100%\'><div class=\'container\'><h4 style=\'height: 50px;\'>`;
        card += element.name;
        card += ` - $`;
        card += element.priceList[0].price;
        card += `</h4> <p style=\'margin: 1px;\'>How long to beat: `;
        card += element.howLongToBeat;
        card += `</p> <div style=\'display: flex; justify-content: space-between; padding-bottom: 15px; padding-top: 15px;\'> <div style=\'display: flex; flex-direction: column; align-items: center;\'> <p style=\'margin: 1px;\'>Metascore</p> <div style=\'border-radius: 15%; background-color: rgba(102, 204, 51, 0.844); color: white; padding: 10px; text-align: center; width: 30%;\'>`;
        card += element.metacritic.metaScore;
        card += `</div> </div> <div style=\'display: flex; flex-direction: column; align-items: center;\'> <p style=\'margin: 1px;\'>User Score</p> <div style=\'border-radius: 50%; background-color: rgba(102, 204, 51, 0.844); color: white; padding: 10px; text-align: center; width: 30%;\'>`;
        card += element.metacritic.userScore;
        card += `</div> </div> </div> </div> </div>`;
        content += card;
    });
    document.getElementById('content').innerHTML = content;
}

getGamesData()