const http = require('http');
const express = require('express');
const path = require('path');

const app = express();

app.use(express.json());
app.use(express.static("public"));
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
  next();
});


app.use('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/index.html'));
  //__dirname : It will resolve to your project folder.
});

const server = http.createServer(app);
const port = 9000;
server.listen(port);
console.debug('Server listening on port ' + port);